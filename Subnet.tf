resource "aws_subnet" "pri_subnet" {
  vpc_id  = aws_vpc.terraform_vpc.id
  cidr_block = "${var.pri_subnet}"
  availability_zone = "us-east-1a"
  tags = {
    Name = "pri_subnet"
  }
}

resource "aws_subnet" "pub_subnet" {
  vpc_id  = aws_vpc.terraform_vpc.id
  cidr_block = "${var.pub_subnet}"
  availability_zone = "us-east-1b"
  tags = {
    Name = "pub_subnet"
  }
}

resource "aws_route_table" "pri_subnet_rt" {
  vpc_id = aws_vpc.terraform_vpc.id
  tags = {
    Name = "privatert"
  }
}

resource "aws_route_table" "pub_subnet_rt" {
  vpc_id = aws_vpc.terraform_vpc.id
tags = {
    Name = "publicrt"
  }
} 

resource "aws_route_table_association" "pub-route" {
    subnet_id = aws_subnet.pub_subnet.id
    route_table_id = aws_route_table.pub_subnet_rt.id
}

resource "aws_route_table_association" "private-route" {
    subnet_id = aws_subnet.pri_subnet.id
  route_table_id = aws_route_table.pri_subnet_rt.id
}

resource "aws_route" "Pub_subent_rt" {
  route_table_id = aws_route_table.pub_subnet_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.gw.id
 
}
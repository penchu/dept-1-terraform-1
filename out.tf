output "VPC_ID" {
 value = aws_vpc.terraform_vpc.id
}

output "pri_subnetid" {
 value = aws_subnet.pri_subnet.id
}

output "pub_subnetid" {
 value = aws_subnet.pub_subnet.id
}

output "pri_subnetidrt" {
 value = aws_route_table.pri_subnet_rt.id
}

output "pub_subnetidrt" {
 value = aws_route_table.pub_subnet_rt.id

}

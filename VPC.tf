resource "aws_vpc" "terraform_vpc" {
  cidr_block = "${var.cidr_block}" 
  instance_tenancy = "default"
assign_generated_ipv6_cidr_block = true
  tags = {
    Name = "My_terraform_vpc"
  }
}
